
# TestButler: IoT testing made easier

> Enabling your software to be directly tested on your hardware in real-life conditions with GitLab CI/CD pipelines

When you build an application to run on edge hardware, testing it on workstations or emulators is just not enough. The factors affecting production deployments of IoT fleets can range from sensor inputs, variants of GPU, electromagnetic interference, internet connectivity, thermal constraints, and even environmental. The only accurate test feedback you can trust in your AI/IoT/Automotive development workflows is when tests run on actual devices.  

<img src="img/logo.png" width=300px />

That's what Testbutler can enable for you! TestButler CLI takes your existing tests, runs them against a specific device target, and outputs backtest results. This is a proof of concept GitLab CI/CD component that does just that. Later, we would like to build this as an AutoDevOps feature that uses GitLab Runners to target devices to run tests against.

## Table of Contents

- [TestButler: IoT testing made easier](#testbutler-iot-testing-made-easier)
  - [Table of Contents](#table-of-contents)
- [Test your code directly on Balena devices with GitLab CI/CD](#test-your-code-directly-on-balena-devices-with-gitlab-cicd)
  - [Features](#features)
- [\[PLEASE CHECK\] Using TestButler right in your GitLab Merge Requests](#please-check-using-testbutler-right-in-your-gitlab-merge-requests)
- [\[Recommended\] Getting Started with TestButler locally](#recommended-getting-started-with-testbutler-locally)
    - [Setting up Testbutler!](#setting-up-testbutler)
    - [Install Testbutler CLI](#install-testbutler-cli)
    - [Running your first test](#running-your-first-test)
- [TestButler in any test scenario](#testbutler-in-any-test-scenario)
  - [System Under Test (SUT): Testing on the device](#system-under-test-sut-testing-on-the-device)
  - [Testing the device (DUT): Testing the device](#testing-the-device-dut-testing-the-device)
- [Future Developments](#future-developments)
- [License](#license)
- [Contribute](#contribute)


# Test your code directly on Balena devices with GitLab CI/CD

Use the TestButler component by adding your required inputs to target your tests on a device target of your choice. To know more, try setting up the [local pipeline](#setting-up-testbutler) and run your first test directly on the device. Please make sure to set all required CI/CD inputs. You can use masked GitLab CI/CD variables to store sensitive API keys as [Project Variables](https://docs.gitlab.com/ee/ci/variables/#for-a-project).

```yaml
include:
  - component: gitlab.com/vipulgupta2048/testbutler/testbutler@<VERSION>
    inputs:
      stage: test
```

| Option         | Description                                                                                                                                      | Required |
| -------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ | :------: |
| target         | Target device to trigger your tests - Ideally where the code is running                                                                          |   Yes    |
| fleet          | Balena cloud fleet name to search multiple targets to test on                                                                                    |    No    |
| release        | [Specific release hash](https://docs.balena.io/learn/deploy/deployment/#balena-push) of Balena fleet that needs to be tested. Defaults to latest |    No    |
| BALENA_API_KEY | [BalenaCloud API Key](https://docs.balena.io/learn/manage/account/#api-keys) to access your BalenaCloud device                                   |   yes    |


## Features 

1. Automate testing across different device types and environments.
2. Freedom to create your own CI/CD pipeline with minimum changes to existing code.
3. No vendor lock, all open-source.
4. Run tests no matter the environment or scenario 
    - Testing on the device (System under test or SUT)
    - Testing the device (Device under test or DUT)
    - Testing integrations & application stack (End-to-end testing or e2e)
5. Native support with the fleet management platform Balena. 

---

# [PLEASE CHECK] Using TestButler right in your GitLab Merge Requests

A demo to showcase how exactly code changes to the software stack get tested right in the merge request on an actual device to provide near-instantaneous feedback to IoT Teams, AI developers, and fleet owners without ever handling the hardware and its many complexities. Testbulter abstracts it all. 

Check out the pipeline for the tests that are running. Testbutler can be integrated into the Gitlab workflow to ensure changes will always work on the hardware in the field, leading to the end of costly production failures in the field. I simulate [making a change](https://gitlab.com/vipulgupta2048/testbutler/-/merge_requests/1) to the software in a merge request and how [automated on device testing](https://gitlab.com/vipulgupta2048/testbutler/-/pipelines/1192023655) works.

<a href="https://www.youtube.com/watch?v=9437h1tTBn0"><img src="img/4.png" width=450px /></a>

In the next PR, you can find another demo using the same CI/CD principles but a reusable CI/CD component in GitLab called TestButler. This [PR was tested and merged](https://gitlab.com/vipulgupta2048/testbutler/-/merge_requests/2) using the Testbutler CI/CD component, which you can find available to use in the CI/CD Catalog. 

![](img/5.png)

---

# [Recommended] Getting Started with TestButler locally

### Setting up Testbutler!

To try Testbutler, you must have a device provisioned on a platform. The current PoC is based on devices running on a fleet management platform called [Balena](https://balena.io) for ease of use. This provides us visibility on what the device is doing, including a way to SSH the device without a local internet connection. 

1. Sign up for a free Balena account and provision a device. You can choose from the 100+ supported Single Board Computers using the [Getting Started](https://www.balena.io/docs/learn/getting-started/generic-amd64/nodejs/) guide.
2. After provisioning a device, you will be on the BalenaCloud dashboard. Please note the UUID of your device.

![](img/3.png)

3. Next, in the Getting Started guide, you would need to push a release onto the device. This will be the application you intend to run. To provision our test device, run the following commands. 

```
cd examples/browser
balena fleet create browser
balena push browser
```

This will push the browser block to a fleet called `browser`. Learn more about the application you just pushed in this [README.md](./examples/browser/readme.md). Once the `balena push` command succeeds, please note the Release commit created at the end. If your device is provisioned to another fleet, move it to the `browser` fleet using the [dashboard](https://docs.balena.io/learn/manage/actions/#move-device-to-another-fleet). Next, use the balenaCLI to generate a new API key using the following command.

```
balena api-key generate "Test Key"
```

Save the API key generated to the `.env` file in the root directory. When added as an input, this will also be used to run the TestButler CI/CD component. Once you finish the preparation steps, your IoT device will be online and ready to run some tests. 


### Install Testbutler CLI

The testbulter CLI is written with NodeJS; hence, make sure there is an LTS version of Node available. We recommend NodeJS 18. You can [install NodeJS](https://nodejs.org/en/download) if you don't already have it installed.

Once NodeJS is installed, install Testbutler using the following commands.

```
git clone https://gitlab.com/vipulgupta2048/testbutler
npm ci
node index.js --help
```

This will install the required dependencies for Testbulter to run and verify that it's working correctly.

### Running your first test

To run any Testbutler test, we need a few things already set up. Here's what we have already completed so far.

1. A target device to test on (UUID of the Device)
2. Test files or test container to run with the application (Release commit which we got after running `balena push`)
3. API keys (configured in the .env folder)

To start running a test on the device you provisioned, run the following command by substituting the device's UUID.

```
node index.js --target <UUID of the device>
```

# TestButler in any test scenario

## System Under Test (SUT): Testing on the device 

In this scenario, the tests will be running on your application's target device—for example, tests to validate if a Node.js app is running optimally on a Raspberry Pi 3. 

![](img/1.jpg)

This pipeline is the quickest to create, requires no additional hardware, and needs minimum changes to the existing test codebase or infrastructure to execute. Here's an example: 


## Testing the device (DUT): Testing the device

In this scenario, the tests run on a separate machine and interact with the target device. For example, testing if a Raspberry Pi's GPIO pins and sensors are functioning as expected.

![](img/2.png)


# Future Developments

1. Remove dependence on Balena to alleviate our vendor lock-in promise further and use GitLab runners to manage and run tests on devices. 
2. Write more examples of using Testbutler in different situations, testing more complex hardware-oriented IoT applications like Signage, GSM, LoRaWAN, etc.
3. Migrate Deploy-to-balena GitHub action as a GitLab CI/CD component for ease of use for GitLab users
4. Supporting Firestarter and Autokit to provide support for complex testing scenarios and device automation
5. Building an AutoDevOps task flow in GitLab that uses GitLab Runners. These GitLab runners will be running on IoT devices targeted for our tests. Using tags, scheduling, and queueing, we can create an ideal Hardware In The Loop pipeline. This way, GitLab's full potential will be unleashed. 


# Inspiration

- Docker Container Testing: https://docs.docker.com/docker-hub/builds/automated-testing/ and testcontainers project
- Uses the browser block: https://github.com/balena-labs-projects/browser
- Balena's test tooling, Firestarter project and Autokit project.

# TestButler Wins GitLab Pitch Competition

The project was part of the GitLab Pitch competition. Only gives me motivation to work on this PoC further!

![](img/6.png)

# License 

All work under this project is licensed under the MIT 2.0 License. 

# Contribute

[Please read about CI/CD and best practices at: https://docs.gitlab.com/ee/ci/components](https://docs.gitlab.com/ee/ci/components)
